/**
 * 
 */
package org.eclipselabs.osgi.jetty.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.eclipse.jetty.servlet.ServletHolder;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

/**
 * A special {@link ServletHolder} that receives a ServcieReference and optains the {@link Servlet} from this.
 * It furthermore Handles the conversion from the init Parameters as described in the OSGi Specs to servletparamteers.
 * @author Juergen Albert
 * @version 1.0.0
 *
 */
public class OsgiWhiteboardServletHolder extends ServletHolder {
	
	private ServiceReference<Servlet> ref;
	private BundleContext ctx;

	/**
	 * Constructor is setting the ServletInstance and the Initparamters
	 * @param ref the {@link Servlet}s {@link ServiceReference}
	 * @param ctx the {@link BundleContext} to use
	 */
	public OsgiWhiteboardServletHolder(ServiceReference<Servlet> ref, BundleContext ctx){
		this.ref = ref;
		this.ctx = ctx;
		
		Servlet servlet = ctx.getService(ref);
		setServlet(servlet);
		setInitParameters(filterInitParameter(ref));
	}
	
	/**
	 * Copies all {@link ServiceReference} properties starting with {@link HttpWhiteboardConstants#HTTP_WHITEBOARD_SERVLET_INIT_PARAM_PREFIX}
	 * and subtracts all the the prefix from the corresponding keys. 
	 * Note that Servlets only take String parameters. Thus every given Parameter that is not a {@link String} will, be converter by calling {@link Object#toString()} 
	 * @param ref the {@link ServiceReference} of the {@link Servlet} held here
	 * @return a {@link Map} containing 0 or more Init parameters
	 */
	private Map<String, String> filterInitParameter(ServiceReference<Servlet> ref) {
		Map<String, String> parameter = new HashMap<>();
		for (String key : ref.getPropertyKeys()) {
			if(key.startsWith(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_INIT_PARAM_PREFIX)){
				parameter.put(key.substring(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_INIT_PARAM_PREFIX.length()), ref.getProperty(key).toString());
			}
		}
		return parameter;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jetty.servlet.ServletHolder#destroyInstance(java.lang.Object)
	 */
	@Override
	public void destroyInstance(Object o) throws Exception {
		super.destroyInstance(o);
		ctx.ungetService(ref);
		System.err.println("instance for " + ref.getProperty("component.name") + " destroyed");
	}
	
}
