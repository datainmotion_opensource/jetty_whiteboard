/**
 * 
 */
package org.eclipselabs.osgi.jetty;

import java.util.Map;

import org.eclipse.jetty.server.Handler;

/**
 * @author jalbert
 *
 */
public class HandlerHolder {

	
	private Handler handler;
	private Map<String, Object> serviceProperties;

	public HandlerHolder(Handler handler, Map<String, Object> serviceProperties) {
		this.handler = handler;
		this.serviceProperties = serviceProperties;
	}

	public Map<String, Object> getServiceProperties() {
		return serviceProperties;
	}

	public void setServiceProperties(Map<String, Object> serviceProperties) {
		this.serviceProperties = serviceProperties;
	}

	public Handler getHandler() {
		return handler;
	}

	
	
}
