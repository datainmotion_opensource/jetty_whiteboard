package org.eclipselabs.osgi.jetty;

import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

final class ServletContextHelperServiceTrackerCustomize implements ServiceTrackerCustomizer<ServletContextHelper, ServletContextHelper> {
	/**
	 * 
	 */
	private final JettyHttpServiceRuntime jettyHttpServiceRuntime;
	private final BundleContext ctx;

	ServletContextHelperServiceTrackerCustomize(JettyHttpServiceRuntime jettyHttpServiceRuntime, BundleContext ctx) {
		this.jettyHttpServiceRuntime = jettyHttpServiceRuntime;
		this.ctx = ctx;
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
	 */
	@Override
	public ServletContextHelper addingService(ServiceReference<ServletContextHelper> reference) {
		ServletContextHelper contextHelper = ctx.getService(reference);
		Map<String, Object> properties = convertToMap(reference);
		try {
			this.jettyHttpServiceRuntime.setServletContextHelper(contextHelper, properties);
		} catch (InvalidSyntaxException e) {
			ctx.ungetService(reference);
			return null;
		}
		return contextHelper;
	}

	private Map<String, Object> convertToMap(ServiceReference<?> reference) {
		Map<String, Object> properties = new HashMap<>();
		for (String key : reference.getPropertyKeys()) {
			properties.put(key, reference.getProperty(key));
		}
		return properties;
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void modifiedService(ServiceReference<ServletContextHelper> reference, ServletContextHelper service) {
		this.jettyHttpServiceRuntime.unsetServletContextHelper(service);
		try {
			this.jettyHttpServiceRuntime.setServletContextHelper(service, convertToMap(reference));
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void removedService(ServiceReference<ServletContextHelper> reference, ServletContextHelper service) {
		this.jettyHttpServiceRuntime.unsetServletContextHelper(service);
	}
}