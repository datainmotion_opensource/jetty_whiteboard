package org.eclipselabs.osgi.jetty;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.server.Handler;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

final class HandlerServiceTrackerCustomize implements ServiceTrackerCustomizer<Handler, Handler> {
	/**
	 * 
	 */
	private final JettyHttpServiceRuntime jettyHttpServiceRuntime;
	private final BundleContext ctx;

	HandlerServiceTrackerCustomize(JettyHttpServiceRuntime jettyHttpServiceRuntime, BundleContext ctx) {
		this.jettyHttpServiceRuntime = jettyHttpServiceRuntime;
		this.ctx = ctx;
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
	 */
	@Override
	public Handler addingService(ServiceReference<Handler> reference) {
		Handler contextHelper = ctx.getService(reference);
		Map<String, Object> properties = convertToMap(reference);
		try {
			this.jettyHttpServiceRuntime.setHandler(contextHelper, properties);
		} catch (InvalidSyntaxException e) {
			ctx.ungetService(reference);
			return null;
		}
		return contextHelper;
	}

	private Map<String, Object> convertToMap(ServiceReference<?> reference) {
		Map<String, Object> properties = new HashMap<>();
		for (String key : reference.getPropertyKeys()) {
			properties.put(key, reference.getProperty(key));
		}
		return properties;
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void modifiedService(ServiceReference<Handler> reference, Handler service) {
		this.jettyHttpServiceRuntime.unsetHandler(service);
		try {
			this.jettyHttpServiceRuntime.setHandler(service, convertToMap(reference));
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void removedService(ServiceReference<Handler> reference, Handler service) {
		this.jettyHttpServiceRuntime.unsetHandler(service);
	}
}