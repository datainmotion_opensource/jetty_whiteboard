package org.eclipselabs.osgi.jetty;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Servlet;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.component.LifeCycle.Listener;
import org.eclipselabs.osgi.jetty.context.ApplicationServletHandler;
import org.eclipselabs.osgi.jetty.context.RootServletContext;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.runtime.HttpServiceRuntime;
import org.osgi.service.http.runtime.HttpServiceRuntimeConstants;
import org.osgi.service.http.runtime.dto.RequestInfoDTO;
import org.osgi.service.http.runtime.dto.RuntimeDTO;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.osgi.util.tracker.ServiceTracker;

/**
 * This class represents a configurable Service that represents a Jetty instance.
 * This implementation supports the HttpWhiteboardRuntime to the outside world.
 * @author Juergen Albert
 *
 */
@Component(name="http.server.jetty", service={}, immediate = true, enabled = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
public class JettyHttpServiceRuntime implements HttpServiceRuntime, Listener{

	private static final Logger LOG = Logger.getLogger(JettyHttpServiceRuntime.class.getName());
	private static final List<String> propertiesNotToCopy = Arrays.asList(new String[]{Constants.SERVICE_PID, Constants.SERVICE_SCOPE, Constants.SERVICE_ID});
	
	private volatile boolean activated = false;
	
	private final ReentrantLock lock = new ReentrantLock();
	
	/**
	 * The wrappped Jetty {@link Server} isntance
	 */
	private Server server = null;

	/**
	 * The {@link ExecutorService} the jetty is running in
	 */
	private ExecutorService executor = null;

	/**
	 * The List of {@link Handler}s 
	 */
	private HandlerCollection serverHandlers = new HandlerCollection(true);
	
	/**
	 * A {@link Map} of connectors with there corresponding endpoint  
	 */
	private Map<String, Connector> connectors;

	private Map<ServletContextHelper, HandlerHolder> contextToHandlerMapping;
	
	/**
	 * List of {@link HandlerHolder} containing all currently used handers  
	 */
	private Map<Handler, HandlerHolder> knownHandlers;

	/**
	 * List of {@link HandlerHolder} containing all currently failed  
	 */
	private List<HandlerHolder> failedHandlers;
	
	/**
	 * The given Service properties to propagate
	 */
	private Map<String, Object> originalProps;
	
	/**
	 * The given Service properties to propagate
	 */
	private Hashtable<String, Object> props;
	
	private volatile RuntimeDTO dto ;

	/**
	 * Our Service Registration for the {@link HttpServiceRuntime}
	 */
	private ServiceRegistration<HttpServiceRuntime> runtimeRegistration;

	private long serviceChangeCount = -1;

	private BundleContext ctx;

	private List<ServiceReference<Servlet>> availableServletServiceReferences;
	private List<ServiceReference<Servlet>> usedServletServiceReferences;

	private ApplicationServletHandler rootServletContextHandler;
	
	private RootServletContext rootServletContext;

	private ServiceTracker<ServletContextHelper, ServletContextHelper> servletContextTracker;

	private ServiceRegistration<ServletContextHelper> rootContextServiceRegistration;

	private ServiceTracker<Servlet, Servlet> servletTracker;
	private ServiceTracker<Handler,Handler> applicationHandlerTracker;
	private String endpoints;
	
	/**
	 * 
	 */
	public JettyHttpServiceRuntime() {
		dto = new RuntimeDTO();
		connectors = new HashMap<>();
		contextToHandlerMapping = new HashMap<>();
		usedServletServiceReferences = new LinkedList<>();
		knownHandlers = new HashMap<>();
	}
	
	@Activate
	public void activate(Map<String, Object> props, BundleContext ctx) throws ConfigurationException, InvalidSyntaxException{
		
		this.ctx = ctx;
		this.server = new Server();
		this.server.addLifeCycleListener(this);
		handlePropertyChange(props, ctx);
		
		server.setHandler(serverHandlers);
		executor = Executors.newCachedThreadPool();
		try {
			executor.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						server.start();
						server.join();
					} catch (Exception e) {
						LOG.log(Level.SEVERE, "Error while starting the Server ", e);
					}
				}
			});
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "A serious error occured while starting the Jetty Server Thread: " + e.getMessage(), e);
		}
		
		//Create a default Root Scope
		Map<String, Object> properties = new HashMap<>();
		properties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, "/");		
		properties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, HttpWhiteboardConstants.HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME);		
		rootServletContext = new RootServletContext();
		rootServletContextHandler = wrapServletContextHelper(rootServletContext, properties);
		setHandler(rootServletContextHandler, properties);
		
		servletTracker = new ServiceTracker<Servlet, Servlet>(ctx, Servlet.class, new ServletServiceTrackerCustomizer(this, ctx));
		
		servletContextTracker = new ServiceTracker<ServletContextHelper, ServletContextHelper>(ctx, ServletContextHelper.class, new ServletContextHelperServiceTrackerCustomize(this, ctx));
		
		applicationHandlerTracker = new ServiceTracker<Handler, Handler>(ctx, Handler.class, new HandlerServiceTrackerCustomize(this, ctx));
		
		applicationHandlerTracker.open();
		servletContextTracker.open();
		servletTracker.open();
	}
	
	private Hashtable<String, Object> extractRuntimeProps(Map<String, Object> props) {
		final Hashtable<String, Object> result = new Hashtable<>();
		
		props.forEach((k, v) ->{
			if(!propertiesNotToCopy.contains(k)){
				result.put(k, v);
			}
		});
		
		return result;
	}

	@Modified
	public void modify(Map<String, Object> props, BundleContext ctx) throws ConfigurationException{
		handlePropertyChange(props, ctx);
		runtimeRegistration.setProperties(this.props);
	}
	public void handlePropertyChange(Map<String, Object> props, BundleContext ctx) throws ConfigurationException{
		final HttpConfiguration httpConfig = new HttpConfiguration();
		
		this.props = extractRuntimeProps(props);
		this.props.put("service.changecount", ++serviceChangeCount);
		this.originalProps = props;
		props.forEach((k,v) -> server.setAttribute(k, v));

		//We have to know what Connectors we have to remove later
		Map<String, Connector> oldConnectors = connectors;
		this.connectors = new HashMap<>();
		
		Object object = props.get(HttpServiceRuntimeConstants.HTTP_SERVICE_ENDPOINT);
		
		StringBuilder endpointString = new StringBuilder("[");
		
		try {
			if(object instanceof String){
				String endpoint = object.toString();
				createConnector(server, httpConfig, endpoint, oldConnectors);
				endpointString.append(endpoint);	
			} else if(object instanceof Collection){
				@SuppressWarnings("unchecked")
				Collection<String> endpoints = (Collection<String>) object;
				for(String endpoint : endpoints){
					createConnector(server, httpConfig, endpoint, oldConnectors);
					if(endpointString.length() > 0) {
						endpointString.append(",");
					}
					endpointString.append(endpoint);
				}
			}
		} catch (MalformedURLException e) {
			throw new ConfigurationException(HttpServiceRuntimeConstants.HTTP_SERVICE_ENDPOINT, "contains a malformed URL", e);
		}
		
		if(object == null){
			String portString = (String) props.get("jetty.http.port");
			if(portString == null){
				portString = (String) props.get("org.osgi.service.http.port");
			}
			
			if(portString == null){
				portString = "8080";
				System.out.println("Neather org.osgi.service.http.port, nor jetty.http.port, nor " + HttpServiceRuntimeConstants.HTTP_SERVICE_ENDPOINT + "given as property. Using default as fallback: 8080");
			}
			String endpoint = "http://0.0.0.0:" + portString;
			this.props.put(HttpServiceRuntimeConstants.HTTP_SERVICE_ENDPOINT, endpoint);
			createConnector(server, httpConfig, null, Integer.parseInt(portString), oldConnectors);
			endpointString.append(endpoint);
		}
		endpointString.append("]");
		endpoints = endpointString.toString();
		oldConnectors.forEach((key, oldConnector) -> server.removeConnector(oldConnector));
		LOG.config("finished removing " + oldConnectors.size() + " connectors");
	}

	/**
	 * Creates a connector to a specific port, interface and or protocol defined by the given endpoint
	 * @param server the server to add the connector to
	 * @param httpConfig the httpConfig to use
	 * @param endpoint the String defining the endpoiint
	 * @param currentConnectors a list off currently configured connectors, to handle connector changes
	 * @throws MalformedURLException thrown if the endpoint is no valid {@link URL}
	 */
	private void createConnector(Server server, HttpConfiguration httpConfig, String endpoint, Map<String, Connector> currentConnectors) throws MalformedURLException {
		LOG.log(Level.INFO, "handling Endpoint " + endpoint);
		URL url = new URL(endpoint);
		String host = url.getHost();
		int port = url.getPort();
		createConnector(server, httpConfig, host, port, currentConnectors);
	}

	/**
	 * Creates a connector to a specific port, interface and or protocol defined by the given endpoint
	 * @param server the server to add the connector to
	 * @param httpConfig the httpConfig to use
	 * @param host the host the connector should listen to. Can be null.
	 * @param port the port the connector should listen on
	 * @param currentConnectors a list off currently configured connectors, to handle connector changes
	 */
	private void createConnector(Server server, HttpConfiguration httpConfig, String host, int port, Map<String, Connector> currentConnectors) {
		String key = host + ":" + port;
		Connector curConnector = currentConnectors.remove(key);
		if(curConnector != null){
			connectors.put(key, curConnector);
			return ;
		}
		
		final ServerConnector connector = new ServerConnector(server,new HttpConnectionFactory(httpConfig));
		HttpConnectionFactory conFactory = new HttpConnectionFactory(httpConfig);
		connector.addConnectionFactory(conFactory);
		if(host != null){
			connector.setHost(host);
		}
		if(port != -1){
			connector.setPort(port);
		}
		
		server.addConnector(connector);
		connectors.put(key, connector);
	}

	/**
	 * Stops the current Jetty
	 */
	@Deactivate
	public void deactivate(){
		try {
			applicationHandlerTracker.close();
			servletContextTracker.close();
			servletTracker.close();
			server.stop();
			server = null;
			if(rootContextServiceRegistration != null) {
				rootContextServiceRegistration.unregister();
				rootContextServiceRegistration = null;
			}
			LOG.log(Level.INFO, "server stoped for endpoints " + endpoints);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error stopping server" + endpoints, e);
		}
	}
	
	public void setServletContextHelper(ServletContextHelper context, Map<String, Object> properties) throws InvalidSyntaxException{
		// The RootServletContext will be already registered at this point, but the service registration appears only after the server 
		// and companion handler is started. Thus we will ignore it here.
		if(!context.equals(rootServletContext)) {
			if(checkWhiteboardTarget(context, properties)) {
				ApplicationServletHandler applicationServletHandler = wrapServletContextHelper(context, properties);
				setHandler(applicationServletHandler, properties);
			}
		} else {
			LOG.info("Here comes the root context for " + endpoints);
		}
	}

	private ApplicationServletHandler wrapServletContextHelper(ServletContextHelper context,
			Map<String, Object> properties) {
		ApplicationServletHandler applicationServletHandler = new ApplicationServletHandler(context, properties);
		HandlerHolder holder = new HandlerHolder(applicationServletHandler, properties);
		contextToHandlerMapping.put(context, holder);
		return applicationServletHandler;
	}
	
	public void unsetServletContextHelper(ServletContextHelper context) {
		if(context.equals(rootServletContext)) {
			LOG.info("                       Here goes the root context " + endpoints);
		}
		HandlerHolder toRemove = contextToHandlerMapping.remove(context);
		if(toRemove != null) {
			unsetHandler(toRemove.getHandler());
		}
	}
	
	public void setHandler(Handler handler, Map<String, Object> handlerProps) throws InvalidSyntaxException{
		LOG.log(Level.INFO, "Add Handler called for " + endpoints);
		if(!checkWhiteboardTarget(handler, handlerProps)) {
			return;
		}
		try {
			lock.lock();
			serverHandlers.setHandlers(pigeonHoleHandler(serverHandlers.getHandlers(), handler));
			handler.addLifeCycleListener(this);
			knownHandlers.put(handler, new HandlerHolder(handler, handlerProps));
			if(handler instanceof ApplicationServletHandler) {
				for (ServiceReference<Servlet> serviceReference : usedServletServiceReferences) {
					Filter filter = null;
					if(null != serviceReference.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT)){
						filter = FrameworkUtil.createFilter((String) serviceReference.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT));
					}
					((ApplicationServletHandler) handler).setServlet(serviceReference, filter, endpoints);
				}
			}
			if(activated) {
				handler.start();
			}
		} catch(Throwable t){
			LOG.log(Level.SEVERE, "An error appeared while updating handlers after the following handler has been added: " + handler + " on " + endpoints, t);
		} finally {
			lock.unlock();
		}
	}
	
	public void unsetHandler(Handler handler){
		try {
			lock.lock();
			if(knownHandlers.containsKey(handler)) {
				handler.stop();
			}
			serverHandlers.removeHandler(handler);
			LOG.log(Level.CONFIG, "removing Handler " + handler.toString());
		} catch(Throwable t){
			LOG.log(Level.SEVERE, "Error removing Handler " + handler.toString() + " on " + endpoints, t);
		} finally {
			lock.unlock();
		}
	}

	private boolean checkWhiteboardTarget(Object handler, Map<String, Object> handlerProps) throws InvalidSyntaxException {
		if(handlerProps.containsKey(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET)){
			try {
				Filter filter = FrameworkUtil.createFilter((String) handlerProps.get(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET));
				if(!filter.matches(originalProps)){
					return false;
				}
			} catch (InvalidSyntaxException e) {
				throw new InvalidSyntaxException(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET + "property of handler " + handler.toString() + " is incorrect", (String) handlerProps.get(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET), e);
			}
		}
		return true;
	}

	public void setServlet(ServiceReference<Servlet> ref) throws InvalidSyntaxException{
		LOG.info("Add Servlet called for Servlet with path " + ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN) + " on " + endpoints);
		try {
			if(null != ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET)){
				Filter filter = FrameworkUtil.createFilter((String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET));
				if(!filter.matches(props)){
					LOG.info("	Servlet with path " + ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN) + " does not match to " + endpoints);
					return;
				}
			}
		} catch (InvalidSyntaxException e) {
			throw new InvalidSyntaxException(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET + "property of handler " + ref.toString() + " is incorrect", (String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET), e);
		}
		usedServletServiceReferences.add(ref);
		Filter filter = null;
		if(null != ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT)){
			filter = FrameworkUtil.createFilter((String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT));
		}
		try {
			lock.lock();
			for (HandlerHolder handlerHolder : contextToHandlerMapping.values()) {
				if(handlerHolder.getHandler() instanceof ApplicationServletHandler) {
					((ApplicationServletHandler) handlerHolder.getHandler()).setServlet(ref, filter, endpoints);
				}
			}
		} finally {
			lock.unlock();
		}
		
		LOG.config("Added Servlet for mapping " + (String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN) + " on " + toString()  );
	}
	
	public void unsetServlet(ServiceReference<Servlet> ref){
		String path = (String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN);
		System.out.println("removed Servlet for mapping " + path);
		if(usedServletServiceReferences.remove(ref)){
			contextToHandlerMapping.values().forEach(handlerHolder -> {
				if(handlerHolder.getHandler() instanceof ApplicationServletHandler) {
					try{
						((ApplicationServletHandler) handlerHolder.getHandler()).unsetServlet(ref);
					} catch (Throwable e) {
						e.printStackTrace();
					}
				}
			});
			
		}
	}
	
	private void updateServiceChangeCount() {
		if(runtimeRegistration != null) {
			props.put("service.changecount", ++serviceChangeCount);
			runtimeRegistration.setProperties(props);
		}
	}

	private Handler[] pigeonHoleHandler(Handler[] handlers, Handler handler) {
		
		Comparator<Handler> handlerSorter = (h1, h2) -> {
			if(!(h1 instanceof ContextHandler)){
				return -1;
			} else if(!(h2 instanceof ContextHandler)){
				return 1;
			}
			
			ContextHandler ctx1 = (ContextHandler) h1;
			ContextHandler ctx2 = (ContextHandler) h2;
			
			return ctx2.getContextPath().compareTo(ctx1.getContextPath());
			
		};
		
		if(handlers == null){
			return new Handler[]{handler};
		}
		
		List<Handler> handlerList = new ArrayList<>(handlers.length +1);
		handlerList.addAll(Arrays.asList(handlers));
		handlerList.add(handler);
		Handler[] result = handlerList.stream().sorted(handlerSorter).toArray(Handler[]::new);
		return result;
	}

	@Override
	public RuntimeDTO getRuntimeDTO() {
		return dto;
	}

	@Override
	public RequestInfoDTO calculateRequestInfoDTO(String path) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void lifeCycleStarting(LifeCycle event) {
		LOG.info(event + "is starting");
		
	}

	@Override
	public void lifeCycleStarted(LifeCycle event) {
		LOG.info("Started is called for: " + event.toString());
		if(event.equals(server)) {
			LOG.info("Server is started");
			activated = true;
			try {
				serverHandlers.start();
				for (Handler handler : knownHandlers.keySet()) {
					handler.start();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			runtimeRegistration = ctx.registerService(HttpServiceRuntime.class, this, new Hashtable<>(props));
		}
		if(event.equals(rootServletContextHandler)) {
			//The RootContext is available. Now we can safly register it and guarantee that the service is reachable
			Map<String, Object> serviceProperties = knownHandlers.get(event).getServiceProperties();
			rootContextServiceRegistration = ctx.registerService(ServletContextHelper.class, rootServletContext, new Hashtable<>(serviceProperties));
		}
		if(knownHandlers.containsKey(event)) {
			LOG.info("Handler has started: " + event.toString());
			updateServiceChangeCount();
		}
	}

	@Override
	public void lifeCycleFailure(LifeCycle event, Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void lifeCycleStopping(LifeCycle event) {
		LOG.info(event.toString() + " is stopping");
	}

	@Override
	public void lifeCycleStopped(LifeCycle event) {
		if(event.equals(server)) {
			LOG.info("Server is stopped");
			runtimeRegistration.unregister();
			runtimeRegistration = null;
			executor.shutdown();
		}
		if(knownHandlers.containsKey(event)) {
			updateServiceChangeCount();
		}
	}



}
