package org.eclipselabs.osgi.jetty.context;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletMapping;
import org.eclipse.jetty.util.resource.PathResource;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipselabs.osgi.jetty.servlet.OsgiWhiteboardServletHolder;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

public class ApplicationServletHandler extends ServletContextHandler{

	private static final Logger LOG = Logger.getLogger(ApplicationServletHandler.class.getName());  
	
	Map<ServiceReference<Servlet>, OsgiWhiteboardServletHolder> refs = new HashMap<>();
	private BundleContext ctx;
	private ServletContextHelper context;
	private Map<String, Object> contextProperties;
	
	public ApplicationServletHandler(ServletContextHelper context, Map<String, Object> properties) {
		contextProperties = properties;
		ctx =  FrameworkUtil.getBundle(getClass()).getBundleContext();
		this.context = context;
		handlePropertiesChanged(properties);
	}
	
	public void handlePropertiesChanged(Map<String, Object> props){
		String path = (String) props.get(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH);
		if(path != null){
			setContextPath(path);
		}
	}
	
	@Override
	public Set<String> getResourcePaths(String path) {
		return context.getResourcePaths(path);
	}
	
	@Override
	public Resource getResource(String path) throws MalformedURLException {
		try {
			return new PathResource(context.getResource(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (URISyntaxException e) {
			String warning = "Malformed URL for resource " + path +  " with message: " + e.getMessage();
			LOG.warning(warning);
			throw new MalformedURLException(warning);
		}
	}
	
	@Override
	public void doScope(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		super.doScope(target, baseRequest, request, response);
	}
	
	/**
	 * @param ref
	 * @param filter
	 * @param endpoints
	 * @throws InvalidSyntaxException
	 */
	public void setServlet(ServiceReference<Servlet> ref, Filter filter, String endpoints) throws InvalidSyntaxException{
		LOG.info("		Add Servlet called for " + ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN) + " on context " + getContextPath() + " for whiteboard with endpoints " + endpoints);
		if(filter != null && !filter.matches(contextProperties)){
			LOG.info("			Servlet with " + ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN) + " on context " + getContextPath() + " for whiteboard with endpoints " + endpoints +  " does not match due to filter " + filter.toString());
			return;
		}
		
		OsgiWhiteboardServletHolder osgiWhiteboardServletHolder = new OsgiWhiteboardServletHolder(ref, ctx);
		refs.put(ref, osgiWhiteboardServletHolder);
		try{
			addServlet(osgiWhiteboardServletHolder, (String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN));
		} catch(Throwable t){
			System.err.println(t.getMessage());
			t.printStackTrace();
		}
		LOG.info("		Added Servlet for mapping " + (String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN) + " on context " + getContextPath()  );
	}
	
	public void unsetServlet(ServiceReference<Servlet> ref){
		String path = (String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN);
		System.out.println("removed Servlet for mapping " + path);
		
		OsgiWhiteboardServletHolder holder = refs.remove(ref);
		if(holder != null){
			try{
				unregister(holder, path);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Handles the removal of a Servlet in a Jetty friendly manner 
	 * @param servlet
	 * @param path
	 */
	public void unregister(OsgiWhiteboardServletHolder servlet, String path) {

		// collect list of remaining mappings and remaining servlets
		final ServletHandler servletHandler = getServletHandler();
		final ServletMapping[] mappings = servletHandler.getServletMappings();
		final List<ServletMapping> remainingMappings = new ArrayList<ServletMapping>(mappings.length);
		for (final ServletMapping mapping : mappings) {
			if (!servlet.getName().equals(mapping.getServletName())) {
				remainingMappings.add(mapping);
			}
		}

		// find servlet to remove
		final ServletHolder[] servlets = servletHandler.getServlets();
		final List<ServletHolder> servletsToRemove = new ArrayList<ServletHolder>(servlets.length);
		final List<ServletHolder> remainingServlets = new ArrayList<ServletHolder>(servlets.length);
		for (final ServletHolder holder : servlets) {
			if (!holder.equals(servlet)) {
				remainingServlets.add(holder);
			} else {
				servletsToRemove.add(holder);
			}
		}

		// update mappings and servlets
		servletHandler.setServlets(remainingServlets.toArray(new ServletHolder[0]));
		remainingServlets.forEach(servletHolder -> {
			try {
				servletHolder.start();
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Could not start ApplicationServletHandler", e);
			}
		});
		servletHandler.setServletMappings(remainingMappings.toArray(new ServletMapping[0]));
		
		// stop removed servlets
		for (final ServletHolder holder : servletsToRemove) {
			try {
				holder.doStop();
			} catch (final Exception e) {
				LOG.log(Level.SEVERE, "Error stopping ServletHolder", e);
			}
		}
		try {
			if(getServer() != null) {
				start();
			}
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Could not start ApplicationServletHandler", e);
		}
	}
}