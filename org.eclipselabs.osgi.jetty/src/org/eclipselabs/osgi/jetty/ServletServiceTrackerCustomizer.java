package org.eclipselabs.osgi.jetty;

import javax.servlet.Servlet;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

final class ServletServiceTrackerCustomizer implements ServiceTrackerCustomizer<Servlet, Servlet> {
	/**
	 * 
	 */
	private final JettyHttpServiceRuntime jettyHttpServiceRuntime;
	private final BundleContext ctx;

	ServletServiceTrackerCustomizer(JettyHttpServiceRuntime jettyHttpServiceRuntime, BundleContext ctx) {
		this.jettyHttpServiceRuntime = jettyHttpServiceRuntime;
		this.ctx = ctx;
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
	 */
	@Override
	public Servlet addingService(ServiceReference<Servlet> reference) {
		try {
			this.jettyHttpServiceRuntime.setServlet(reference);
		} catch (InvalidSyntaxException e) {
			return null;
		}
		return ctx.getService(reference);
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void modifiedService(ServiceReference<Servlet> reference, Servlet service) {
		this.jettyHttpServiceRuntime.unsetServlet(reference);
		try {
			this.jettyHttpServiceRuntime.setServlet(reference);
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void removedService(ServiceReference<Servlet> reference, Servlet service) {
		this.jettyHttpServiceRuntime.unsetServlet(reference);
	}
}