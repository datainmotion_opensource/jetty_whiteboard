package org.eclipselabs.osgi.jetty.test;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

@Component(
    service=Servlet.class,
    property= HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN + "=/echo",
    scope=ServiceScope.PROTOTYPE)
public class EchoServlet extends HttpServlet {
 
    private static final long serialVersionUID = 1L;
 
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
 
        String input = req.getParameter("value");
        if (input == null) {
            throw new IllegalArgumentException("input can not be null");
        }
        resp.setContentType("text/html");
        resp.getWriter().write(
            "echo " + input + "");
        }
 
}