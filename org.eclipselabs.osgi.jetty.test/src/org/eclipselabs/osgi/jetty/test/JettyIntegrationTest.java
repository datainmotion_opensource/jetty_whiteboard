package org.eclipselabs.osgi.jetty.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.server.Handler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.runtime.HttpServiceRuntime;
import org.osgi.service.http.runtime.HttpServiceRuntimeConstants;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

@RunWith(MockitoJUnitRunner.class)
public class JettyIntegrationTest {

	private final BundleContext context = FrameworkUtil.getBundle(JettyIntegrationTest.class).getBundleContext();

	private ConfigurationAdmin configAdmin = null;

	List<ServiceReference<?>> referencesToCleanup = new LinkedList<>();
	List<ServiceRegistration<?>> registrationsToCleanup = new LinkedList<>();

	private Configuration configuration;

	List<Configuration> configsToClean = new LinkedList<>();
	List<ServiceChecker> waitForRemoves = new LinkedList<>();
	
	private HttpClient client;
	
	@Before
	public void before() throws Exception {
		
		System.out.println();
		System.out.println("============================================================");
		System.out.println("=================== setup " + this.toString() + "======");
		System.out.println("============================================================");
		System.out.println();
		
		ServiceReference<ConfigurationAdmin> serviceReference = context.getServiceReference(ConfigurationAdmin.class);
		assertNotNull(serviceReference);
		referencesToCleanup.add(serviceReference);
		configAdmin = context.getService(serviceReference);
		
		//Setup a Servcer
		try {
			configuration = configAdmin.createFactoryConfiguration("http.server.jetty", "?");
			Dictionary<String, String> props = new Hashtable<>();
			props.put("org.osgi.service.http.port", "8091");
			props.put("test.id", "initial");
			configuration.update(props);
			configsToClean.add(configuration);
		} catch (IOException e) {
			assertNull(e);
		}
		client = new HttpClient();
		client.start();
		
		System.out.println();
		System.out.println("============================================================");
		System.out.println("=================== setup finished =========================");
		System.out.println("============================================================");
		System.out.println();
	}

	@After
	public void after() throws Exception {
		System.out.println();
		System.out.println("============================================================");
		System.out.println("=================== Cleaning up ============================");
		System.out.println("============================================================");
		System.out.println();
		try {
			configsToClean.forEach(c -> {
				try {
					c.delete();
				} catch (IOException e) {
					assertNull(e);
				}
			});
		} catch (Exception e) {
			assertNull(e);
		}
		
		configAdmin = null;
		referencesToCleanup.forEach(s -> context.ungetService(s));
		registrationsToCleanup.forEach(s -> s.unregister());
		
		waitForRemoves.forEach(sc -> {
			try {
				sc.waitRemove();
			} catch (InterruptedException e) {
				assertNull(e);
			} finally {
				sc.stop();
			}
		});
		
		client.stop();
		
		System.out.println();
		System.out.println("============================================================");
		System.out.println("=================== Cleaning finished ======================");
		System.out.println("============================================================");
		System.out.println();
	}

	@Test
	public void testInitialSetup() throws Exception {
		
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t=================== testInitialSetup =======================");
		System.out.println("\t============================================================");
		System.out.println();
		
		ServiceChecker<HttpServiceRuntime> serviceRuntime = createdCheckerTrackedForCleanUp("(test.id=initial)", context);
		
		serviceRuntime.start();
		
		assertTrue(serviceRuntime.waitCreate());
		
		CountDownLatch latch = new CountDownLatch(1);
//		latch.await(5, TimeUnit.SECONDS);
		
		ContentResponse response = client.GET("http://localhost:8091/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t=================== end testInitialSetup ===================");
		System.out.println("\t============================================================");
		System.out.println();
	}

	private <T extends Object> ServiceChecker<T> createdCheckerTrackedForCleanUp(Class serviceClass, BundleContext context) {
		ServiceChecker<Handler> checker = new ServiceChecker<>(serviceClass, context);

		
		checker.setCreateCount(1);
		checker.setDeleteCount(1);
		checker.setCreateTimeout(5);
		checker.setDeleteTimeout(5);
		waitForRemoves.add(checker);
		return (ServiceChecker<T>) checker;
	}

	private <T extends Object> ServiceChecker<T>  createdCheckerTrackedForCleanUp(String filter, BundleContext context) throws InvalidSyntaxException {
		ServiceChecker<? extends Object> checker = new ServiceChecker<>(filter, context);
		
		checker.setCreateCount(1);
		checker.setDeleteCount(1);
		checker.setCreateTimeout(5);
		checker.setDeleteTimeout(5);
		waitForRemoves.add(checker);
		return (ServiceChecker<T>) checker;
	}

	@Test
	public void testAdditionalContext() throws Exception {
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t=================== testAdditionalContext ==================");
		System.out.println("\t============================================================");
		System.out.println();
		
		ServiceChecker<HttpServiceRuntime> serviceRuntime = createdCheckerTrackedForCleanUp("(test.id=initial)", context);

		serviceRuntime.start();
		
		assertTrue(serviceRuntime.waitCreate());
		
		//look if the servlet is available on the default context
		ContentResponse response = client.GET("http://localhost:8091/echo?value=test");
		assertEquals("echo test",response.getContentAsString());

		ServletContextHelper newContext = new ServletContextHelper() {
		};
		
		Dictionary<String, String> ctxProps = new Hashtable<>();
		ctxProps.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, "/test");
		ctxProps.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, "test");
		
		serviceRuntime.stop();
		serviceRuntime.setModifyCount(1);
		serviceRuntime.setModifyTimeout(50000);
		serviceRuntime.start();
		registrationsToCleanup.add(context.registerService(ServletContextHelper.class, newContext, ctxProps));
		
		assertTrue(serviceRuntime.waitModify());
		response = client.GET("http://localhost:8091/test/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
	
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t=============== end testAdditionalContext ==================");
		System.out.println("\t============================================================");
		System.out.println();
	}
	
	@Test
	public void testAdditionalContextAndRuntime() throws Exception {
		
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t=============== testAdditionalContextAndRuntime ============");
		System.out.println("\t============================================================");
		System.out.println();
		
		ServiceChecker<HttpServiceRuntime> initialServiceRuntime = createdCheckerTrackedForCleanUp("(test.id=initial)", context);
		
		initialServiceRuntime.start();
		
		assertTrue(initialServiceRuntime.waitCreate());
		
		
		//look if the servlet is available on the default context
		ContentResponse response = client.GET("http://localhost:8091/echo?value=test");
		assertEquals("echo test",response.getContentAsString());

		ServletContextHelper newContext = new ServletContextHelper() {
		};
		
		Dictionary<String, String> ctxProps = new Hashtable<>();
		ctxProps.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, "/test");
		ctxProps.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, "test");

		// We have to check if the context will be registered and if the runtime will call the servicecounter update before we try to contact the contexts servlets
		ServiceChecker<ServletContextHelper> servletContextChecker = createdCheckerTrackedForCleanUp("(" + HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME + "=test)", context);
		servletContextChecker.start();
		initialServiceRuntime.stop();
		initialServiceRuntime.setModifyCount(1);
		initialServiceRuntime.setModifyTimeout(5000);
		initialServiceRuntime.start();
		
		registrationsToCleanup.add(context.registerService(ServletContextHelper.class, newContext, ctxProps));
		
		assertTrue("Context Service was not registered", servletContextChecker.waitCreate());
		assertTrue("The runtime has not anounced the expected config change", initialServiceRuntime.waitModify());
		response = client.GET("http://localhost:8091/test/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		//Setup a Server
		try {
			configuration = configAdmin.createFactoryConfiguration("http.server.jetty", "?");
			Dictionary<String, String> props = new Hashtable<>();
			props.put("org.osgi.service.http.port", "8092");
			props.put("test.id", "additionalServer");
			configuration.update(props);
		} catch (IOException e) {
			assertNull(e);
		}
		
		ServiceChecker<HttpServiceRuntime> additionalServiceRuntime = createdCheckerTrackedForCleanUp("(test.id=additionalServer)", context);
		additionalServiceRuntime.start();
		assertTrue(additionalServiceRuntime.waitCreate());
		
		response = client.GET("http://localhost:8092/test/echo?value=test");
		assertEquals("echo test",response.getContentAsString());

		response = client.GET("http://localhost:8092/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t=========== end testAdditionalContextAndRuntime ============");
		System.out.println("\t============================================================");
		System.out.println();
		
	}
	
	@Test
	public void testEndpoints() throws Exception {
		
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t==================== testEndpoints =========================");
		System.out.println("\t============================================================");
		System.out.println();
		
		Logger log = Logger.getLogger("JettyIntegrationTest.testEndpoints");
		
		ServiceChecker<HttpServiceRuntime> serviceRuntime = createdCheckerTrackedForCleanUp("(test.id=initial)", context);
		
		serviceRuntime.start();
		
		assertTrue(serviceRuntime.waitCreate());
		
		//look if the servlet is available on the default context
		ContentResponse response = client.GET("http://localhost:8091/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		configuration = configAdmin.createFactoryConfiguration("http.server.jetty", "?");
		
		configsToClean.add(configuration);
		
		ServiceChecker<HttpServiceRuntime> additionalServiceRuntime = createdCheckerTrackedForCleanUp("(test.id=endpoints)", context);
		additionalServiceRuntime.setModifyCount(1);
		additionalServiceRuntime.setModifyTimeout(5);
		additionalServiceRuntime.start();
		
		log.info("Setting up 3 Endpoints");
		Dictionary<String, Object> props = new Hashtable<>();
		List<String> endpoints = new ArrayList<>();
		endpoints.add("http://localhost:8092");
		endpoints.add("http://127.0.0.1:8093");
		endpoints.add("http://0.0.0.0:8094");
		props.put(HttpServiceRuntimeConstants.HTTP_SERVICE_ENDPOINT, endpoints);
		props.put("test.id", "endpoints");
		configuration.update(props);
		
		log.info("Awaiting Setup to apply");
		assertTrue(additionalServiceRuntime.waitCreate());
		
		
		response = client.GET("http://localhost:8092/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		response = client.GET("http://localhost:8093/echo?value=test");
		assertEquals("echo test",response.getContentAsString());

		response = client.GET("http://localhost:8094/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		//To make sure, test if the default service is still available
		//look if the servlet is available on the default context
		response = client.GET("http://localhost:8091/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		endpoints.remove(2);
		additionalServiceRuntime.stop();
		additionalServiceRuntime.start();
		props.put(HttpServiceRuntimeConstants.HTTP_SERVICE_ENDPOINT, endpoints);
		log.info("Setting up 2 Endpoints");
		configuration.update(props);
		
		log.info("Awaiting Setup to apply");
		assertTrue(additionalServiceRuntime.waitModify());

		response = client.GET("http://localhost:8092/echo?value=test");
		assertEquals("echo test",response.getContentAsString());
		
		response = client.GET("http://localhost:8093/echo?value=test");
		assertEquals("echo test",response.getContentAsString());

		try{
			response = client.GET("http://localhost:8094/echo?value=test");
			assertNotEquals("echo test",response.getContentAsString());
		} catch (ExecutionException e) {
			assertTrue("Expected a Connection Error, but was " + e.getCause(), e.getCause() instanceof ConnectException);
		}
		
		System.out.println();
		System.out.println("\t============================================================");
		System.out.println("\t================ end testEndpoints =========================");
		System.out.println("\t============================================================");
		System.out.println();
	}

}